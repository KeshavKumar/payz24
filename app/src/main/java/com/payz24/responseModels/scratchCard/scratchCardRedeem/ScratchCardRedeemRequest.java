package com.payz24.responseModels.scratchCard.scratchCardRedeem;

import android.content.Context;

import com.payz24.responseModels.baseclasses.BaseRequest;

public class ScratchCardRedeemRequest extends BaseRequest {
    private String scratchCard_id;
    public ScratchCardRedeemRequest(Context context) {
        super(context);
    }

    public String getScratchCard_id() {
        return scratchCard_id;
    }

    public void setScratchCard_id(String scratchCard_id) {
        this.scratchCard_id = scratchCard_id;
    }
}
