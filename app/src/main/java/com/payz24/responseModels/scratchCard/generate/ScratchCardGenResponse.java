package com.payz24.responseModels.scratchCard.generate;

import com.payz24.responseModels.baseclasses.BaseResponse;

public class ScratchCardGenResponse extends BaseResponse {

    private String scratchCard;
    private String amountEarn;
    private String scratchMsg;

    public String getScratchCard() {
        return scratchCard;
    }

    public void setScratchCard(String scratchCard) {
        this.scratchCard = scratchCard;
    }

    public String getAmountEarn() {
        return amountEarn;
    }

    public void setAmountEarn(String amountEarn) {
        this.amountEarn = amountEarn;
    }

    public String getScratchMsg() {
        return scratchMsg;
    }

    public void setScratchMsg(String scratchMsg) {
        this.scratchMsg = scratchMsg;
    }
}
