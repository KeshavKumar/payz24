package com.payz24.responseModels.scratchCard;


import com.payz24.responseModels.baseclasses.BaseResponse;

import java.util.List;

public class ScratchCardResponse extends BaseResponse {
    public int totalReward;
    public List<ScratchItem> scratchList;

    public int getTotalRewards() {
        return totalReward;
    }

    public void setTotalRewards(int totalRewards) {
        this.totalReward = totalRewards;
    }

    public List<ScratchItem> getScratchList() {
        return scratchList;
    }

    public void setScratchList(List<ScratchItem> scratchList) {
        this.scratchList = scratchList;
    }
}
