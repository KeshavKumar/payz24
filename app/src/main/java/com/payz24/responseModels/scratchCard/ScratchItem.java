package com.payz24.responseModels.scratchCard;

import java.io.Serializable;

/**
 * Created by Priyanka on 11-07-2018.
 */

public class ScratchItem implements Serializable{

    private String id;
    private String amountEarn;
    private String scratchMsg;
    private String paidDate;
    private String paidTo;
    private String paidType;
    private boolean isCardScratch;

    public ScratchItem(String amountEarn){
        this.amountEarn = amountEarn;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmountEarn() {
        return amountEarn;
    }

    public void setAmountEarn(String amountEarn) {
        this.amountEarn = amountEarn;
    }

    public String getScratchMsg() {
        return scratchMsg;
    }

    public void setScratchMsg(String scratchMsg) {
        this.scratchMsg = scratchMsg;
    }

    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(String paidDate) {
        this.paidDate = paidDate;
    }

    public String getPaidTo() {
        return paidTo;
    }

    public void setPaidTo(String paidTo) {
        this.paidTo = paidTo;
    }

    public String getPaidType() {
        return paidType;
    }

    public void setPaidType(String paidType) {
        this.paidType = paidType;
    }

    public boolean isCardScratch() {
        return isCardScratch;
    }

    public void setCardScratch(boolean cardScratch) {
        isCardScratch = cardScratch;
    }
}
