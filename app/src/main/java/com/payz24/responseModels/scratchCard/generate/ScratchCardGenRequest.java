package com.payz24.responseModels.scratchCard.generate;

import android.content.Context;

import com.payz24.responseModels.baseclasses.BaseRequest;

public class ScratchCardGenRequest extends BaseRequest {

    private String transaction_id;
    private String paidType;

    public ScratchCardGenRequest(Context context) {
        super(context);
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getPaidType() {
        return paidType;
    }

    public void setPaidType(String paidType) {
        this.paidType = paidType;
    }
}
