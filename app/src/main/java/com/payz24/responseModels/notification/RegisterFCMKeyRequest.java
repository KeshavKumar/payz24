package com.payz24.responseModels.notification;

import android.content.Context;

import com.payz24.responseModels.baseclasses.BaseRequest;

public class RegisterFCMKeyRequest extends BaseRequest {

    private String fcm_key;
    private String device_id;

    public RegisterFCMKeyRequest(Context context) {
        super(context);
    }

    public String getFcm_key() {
        return fcm_key;
    }

    public void setFcm_key(String fcm_key) {
        this.fcm_key = fcm_key;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}
