package com.payz24.responseModels.baseclasses;

import android.content.Context;

import com.payz24.R;
import com.payz24.utils.Constants;
import com.payz24.utils.PreferenceConnector;

public abstract class BaseRequest {
    private String key;
    private int user_id;

    public BaseRequest(Context context) {
        key = Constants.UTILITY_API_KEY;
        user_id = Integer.parseInt(PreferenceConnector.readString(context, context.getString(R.string.user_id),"0"));
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
