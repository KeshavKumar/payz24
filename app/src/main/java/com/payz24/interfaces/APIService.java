package com.payz24.interfaces;

import com.payz24.responseModels.baseclasses.BaseResponse;
import com.payz24.responseModels.notification.RegisterFCMKeyRequest;
import com.payz24.responseModels.scratchCard.ScratchCardRequest;
import com.payz24.responseModels.scratchCard.ScratchCardResponse;
import com.payz24.responseModels.scratchCard.generate.ScratchCardGenRequest;
import com.payz24.responseModels.scratchCard.generate.ScratchCardGenResponse;
import com.payz24.responseModels.scratchCard.scratchCardRedeem.ScratchCardRedeemRequest;
import com.payz24.responseModels.scratchCard.scratchCardRedeem.ScratchCardRedeemResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIService {

    String WS_STATUS_SUCCESS = "success";

    int WS_STATUS_SUCCESS_CODE = 200;

    @POST("cards")
    Observable<ScratchCardResponse> getScratchCardList(@Body ScratchCardRequest request);

    @POST("newcard")
    Observable<ScratchCardGenResponse> generateScratchCard(@Body ScratchCardGenRequest request);

    @POST("redeem")
    Observable<ScratchCardRedeemResponse> scratchCardRedeem(@Body ScratchCardRedeemRequest request);

    @POST("fcmreg")
    Observable<BaseResponse> registerFCMKey(@Body RegisterFCMKeyRequest request);
}
