package com.payz24.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.payz24.activities.recharge.NewMobileRechargeActivity;

import static android.support.v4.app.ActivityCompat.requestPermissions;
import static android.support.v4.content.ContextCompat.checkSelfPermission;


/**
 * Created by mahesh on 1/8/2018.
 */

public class Permissions {

    final static public int PERMISSIONS_REQUEST_READ_PHONE_STATE = 1005;
    public static boolean checkPermissionForAccessExternalStorage(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        else
            return true;
    }

    public static void requestPermissionForAccessExternalStorage(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION);
        }
    }

    public static boolean checkPermissionForReadContacts(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
        else
            return true;
    }

    public static void requestPermissionForReadContacts(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, Constants.REQUEST_CODE_FOR_READ_CONTACTS_PERMISSION);


        }
    }

    public static String getDeviceId(Activity activity){
        TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = "";
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(activity,new String[]{Manifest.permission.READ_PHONE_STATE},
                        PERMISSIONS_REQUEST_READ_PHONE_STATE);
            } else {
                deviceId = tm.getDeviceId();
            }
        } else {
            deviceId = tm.getDeviceId();
        }

        return deviceId;
    }
}
