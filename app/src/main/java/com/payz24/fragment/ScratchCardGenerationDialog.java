package com.payz24.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.payz24.R;
import com.payz24.http.SmsHttpClient;
import com.payz24.interfaces.HttpReqResCallBack;
import com.payz24.utils.Constants;
import com.payz24.utils.PreferenceConnector;

import java.util.HashMap;
import java.util.Map;

public class ScratchCardGenerationDialog extends DialogFragment implements HttpReqResCallBack {

    public static final String TAG = "FullScreenDialog";
    private Dialog mDialog;
    private ImageView mImgScratchCard;
    private Button mBtnOk;
    private DialogFragmentListner mDialogListner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle state) {
        super.onCreateView(inflater, parent, state);

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_scratch_card_gen_dialog, parent, false);
        mImgScratchCard = view.findViewById(R.id.imgScratchCardAnim);
        mBtnOk = view.findViewById(R.id.btnOk);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        mDialog = getDialog();
        if (mDialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            mDialog.getWindow().setLayout(width, height);
        }

        ScaleAnimation fade_in =  new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        fade_in.setDuration(3000);     // animation duration in milliseconds
        fade_in.setFillAfter(true);    // If fillAfter is true, the transformation that this animation performed will persist when it is finished.
        mImgScratchCard.startAnimation(fade_in);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //stop the animation
                mImgScratchCard.setAnimation(null);
                if (mDialog != null && mDialog.isShowing()) {
                    if (mDialogListner != null) {
                        sendNotification();
                        serviceCallForSMSGateWay();
                        mDialogListner.onDialogClick();
                    }
                    mDialog.dismiss();
                }
            }
        }, 10000);

        mBtnOk.setOnClickListener(v->{
            if(mDialogListner!=null){
                mDialog.dismiss();
                sendNotification();
                serviceCallForSMSGateWay();
                mDialogListner.onDialogClick();
            }
        });

    }

    public void setDialogListener(DialogFragmentListner listener){
        mDialogListner = listener;
    }



    public interface DialogFragmentListner{
        void onDialogClick();
    }

    void sendNotification(){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), "aas")
                .setSmallIcon(R.drawable.payzlogo)
                .setContentTitle("Scratch Card Generated")
                .setContentText("Scratch Card Generated for your last transaction. Please check in Scratch card list.")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Scratch Card Generated. Please check in Scratch card list."))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
        notificationManager.notify(1, mBuilder.build());

    }

    private void serviceCallForSMSGateWay() {
        String message = "Scratch Card Generated for your last transaction. Please check in Scratch card list.";
        Map<String, String> mapOfRequestParams = new HashMap<>();
        String userPhoneNumber = PreferenceConnector.readString(getContext(), getString(R.string.user_mobile_number), "0");
        mapOfRequestParams.put(Constants.SMS_PARAM_PHONE, userPhoneNumber);
        mapOfRequestParams.put(Constants.SMS_PARAM_MESSAGE, message);
        SmsHttpClient smsHttpClient = new SmsHttpClient(getContext());
        smsHttpClient.callBack = this;
        smsHttpClient.getJsonForType(mapOfRequestParams);
    }
    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType){
            case Constants.SERVICE_SMS:
                //Toast.makeText(getContext(),"SMS sent successfully",Toast.LENGTH_SHORT).show();
                Log.e("SCGenerationDialog","Scratch card SMS sent successfully");
                break;
        }
    }

}
