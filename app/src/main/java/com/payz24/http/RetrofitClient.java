package com.payz24.http;

import android.util.Log;

import com.payz24.BuildConfig;
import com.payz24.interfaces.APIService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;
    private static String mBaseUrl = "";

    public static final Executor THREAD_POOL_EXECUTOR
            = Executors.newFixedThreadPool(5);

    public static APIService getAPIService(String baseUrl) {
        return RetrofitClient.getClient(baseUrl).create(APIService.class);
    }

    public static Retrofit getClient(String baseUrl) {

        if (retrofit == null || !baseUrl.equalsIgnoreCase(mBaseUrl)) {
            Log.e("RetrofitClient", "baseUrl  === "+baseUrl);
            mBaseUrl = baseUrl;
            //TODO for logging purpose
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            // add logging as last interceptor
            if(BuildConfig.DEBUG) {
                httpClient.addInterceptor(logging);  // <-- this is the important line!
            }

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())  //to add logging code
                    .callbackExecutor(THREAD_POOL_EXECUTOR) // for parallel calls
                    .build();

        }
        return retrofit;
    }

}
