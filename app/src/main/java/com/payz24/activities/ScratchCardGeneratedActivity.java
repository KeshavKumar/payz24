package com.payz24.activities;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.payz24.R;
import com.payz24.http.RetrofitClient;
import com.payz24.interfaces.APIService;
import com.payz24.responseModels.scratchCard.generate.ScratchCardGenRequest;
import com.payz24.responseModels.scratchCard.generate.ScratchCardGenResponse;
import com.payz24.utils.Constants;
import com.payz24.utils.NetworkDetection;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class ScratchCardGeneratedActivity extends BaseActivity {

    private ImageView mImgScratchCardGen;
    private APIService mAPIService;
    private CompositeDisposable mCompositeDisposable;
    private NetworkDetection mNetworkDetection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scratch_card_generated);
        mNetworkDetection = new NetworkDetection();
        mImgScratchCardGen = findViewById(R.id.imgScratchCardGen);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.rotation);
        mImgScratchCardGen.startAnimation(anim);
        
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mNetworkDetection.isWifiAvailable(this) || mNetworkDetection.isMobileNetworkAvailable(this)) {
            generateScratchCard();
        } else {
            Toast.makeText(this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void generateScratchCard() {
        showProgressBar();
        ScratchCardGenRequest request = new ScratchCardGenRequest(this);
        request.setTransaction_id("A086704");
        request.setPaidType("Flight");
        mAPIService = RetrofitClient.getAPIService(Constants.SCRATCH_CARD_BASE_URL);
        mCompositeDisposable.add(mAPIService.generateScratchCard(request).
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResponse, this::handleError));

    }

    private void handleError(Throwable error) {
        closeProgressbar();
        Toast.makeText(this, "Error == " + error.getLocalizedMessage(), Toast.LENGTH_SHORT)
                .show();
    }

    private void handleResponse(ScratchCardGenResponse response) {
        closeProgressbar();
        Toast.makeText(this, "Generated Scratch Card == " + response.getAmountEarn()
                + " Msg == " + response.getScratchCard(), Toast.LENGTH_SHORT)
                .show();
    }
}
