package com.payz24.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.payz24.R;
import com.payz24.adapter.ScratchAdapter;
import com.payz24.http.RetrofitClient;
import com.payz24.interfaces.APIService;
import com.payz24.responseModels.scratchCard.ScratchCardRequest;
import com.payz24.responseModels.scratchCard.ScratchCardResponse;
import com.payz24.responseModels.scratchCard.ScratchItem;
import com.payz24.responseModels.scratchCard.scratchCardRedeem.ScratchCardRedeemRequest;
import com.payz24.responseModels.scratchCard.scratchCardRedeem.ScratchCardRedeemResponse;
import com.payz24.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ScratchCardActivity extends BaseActivity implements ScratchAdapter.OnCardViewClickListener {

    public final String TAG = "ScratchCardActivity";
    public static final String INTENT_SCRATCH_CARD = "ScratchCard";
    public static final String INTENT_SCRATCH_CARD_POISTION = "ScratchCardPosition";
    public static final String INTENT_EMPTY_SCRATCH_CARD = "EmptyScratchCard";
    public static final String INTENT_SCRATCH_CARD_CHANGED = "ScratchCardChanged";

    private RecyclerView recyclerView;
    private ScratchAdapter adapter;
    private List<ScratchItem> mScratchItemList;
    private Toolbar mToolbar;
    private LinearLayout linearLayout;
    private APIService mAPIService;
    private CompositeDisposable mCompositeDisposable;
    private TextView mTxtTotalRewards, mTxtErrorMsg;
    private boolean isFromRedeem = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scratch_card_list);
        mCompositeDisposable = new CompositeDisposable();

        mTxtTotalRewards = findViewById(R.id.txtAmount);
        mTxtErrorMsg = findViewById(R.id.txtErrorMsg);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        linearLayout = findViewById(R.id.linearLayout);
        AppBarLayout appBarLayout = findViewById(R.id.app_bar);

        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    collapsingToolbarLayout.setTitle(getString(R.string.scratch_card_reward));
                    linearLayout.setVisibility(View.INVISIBLE);
                } else if (verticalOffset == 0) {
                    // Expanded
                    collapsingToolbarLayout.setTitle(" ");
                    linearLayout.setVisibility(View.VISIBLE);
                } else {
                    // Somewhere in between
                    collapsingToolbarLayout.setTitle(" ");
                    linearLayout.setVisibility(View.VISIBLE);
                }

                /*if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }

                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Scratch Cards");
                    isShow = true;
                    linearLayout.setVisibility(View.INVISIBLE);
                } else if (isShow) {
                    linearLayout.setVisibility(View.VISIBLE);
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }*/

            }
        });


        mToolbar.findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setSupportActionBar(mToolbar);

        recyclerView = (RecyclerView) findViewById(R.id.rvRecyclerView);
        mScratchItemList = new ArrayList<>();
        adapter = new ScratchAdapter(this, mScratchItemList, this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        getScratchCardList();
    }

    private void getScratchCardList() {
        showProgressBar();
        ScratchCardRequest request = new ScratchCardRequest(this);

        mAPIService = RetrofitClient.getAPIService(Constants.SCRATCH_CARD_BASE_URL);
        mCompositeDisposable.add(mAPIService.getScratchCardList(request).
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResponse, this::handleError));

    }

    private void handleError(Throwable error) {
        closeProgressbar();
        findViewById(R.id.viewInclude).setVisibility(View.GONE);
        findViewById(R.id.errorMsgLayout).setVisibility(View.VISIBLE);
        mTxtErrorMsg.setText(R.string.status_error);
        Toast.makeText(this, "Error == " + error.getLocalizedMessage(), Toast.LENGTH_SHORT)
                .show();
    }

    private void handleResponse(ScratchCardResponse scratchCardResponse) {

        if (scratchCardResponse.getStatus().equalsIgnoreCase(APIService.WS_STATUS_SUCCESS)
                && scratchCardResponse.getStatusCode() == APIService.WS_STATUS_SUCCESS_CODE) {
            if (isFromRedeem && !scratchCardResponse.getScratchList().isEmpty()) {
                mTxtTotalRewards.setText(scratchCardResponse.getTotalRewards() + "");
                isFromRedeem = false;
            } else {
                if (scratchCardResponse.getScratchList().isEmpty()) {
                    findViewById(R.id.viewInclude).setVisibility(View.GONE);
                    findViewById(R.id.errorMsgLayout).setVisibility(View.VISIBLE);
                    mTxtErrorMsg.setText(R.string.scratch_card_empty_msg);

                } else {
                    findViewById(R.id.viewInclude).setVisibility(View.VISIBLE);
                    findViewById(R.id.errorMsgLayout).setVisibility(View.GONE);
                    mTxtTotalRewards.setText(scratchCardResponse.getTotalRewards() + "");

                    Log.e(TAG, "getScratchList === " + scratchCardResponse.getScratchList().size());
                    mScratchItemList.addAll(scratchCardResponse.getScratchList());
                    Log.e(TAG, "mScratchItemList === " + mScratchItemList.size());
                    adapter.notifyDataSetChanged();
                }
            }

        } else {
            findViewById(R.id.viewInclude).setVisibility(View.GONE);
            findViewById(R.id.errorMsgLayout).setVisibility(View.VISIBLE);
            mTxtErrorMsg.setText(R.string.status_error);
        }
        closeProgressbar();
        Log.e(TAG, "scratchItem clicked === " + scratchCardResponse.getStatus());
    }

    @Override
    public void onCardViewClick(ScratchItem scratchItem, int position) {
        Log.e(TAG, "scratchItem position === " + position);
        Log.e(TAG, "scratchItem clicked === " + scratchItem.getAmountEarn());
        Intent intent = new Intent(this, ScratchCardFullActivity.class);
        intent.putExtra(INTENT_SCRATCH_CARD, scratchItem);
        intent.putExtra(INTENT_SCRATCH_CARD_POISTION, position);

        startActivityForResult(intent, 1);

    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                if (data != null) {
                    ScratchItem item = (ScratchItem) data.getSerializableExtra(INTENT_SCRATCH_CARD);
                    int pos = (int) data.getIntExtra(INTENT_SCRATCH_CARD_POISTION, -1);
                    boolean isZeroAmt = data.getBooleanExtra(INTENT_EMPTY_SCRATCH_CARD, false);
                    boolean isDataChanged = data.getBooleanExtra(INTENT_SCRATCH_CARD_CHANGED, false);
                    if (pos != -1 && isDataChanged) {
                        scratchCardRedeem(item);
                        if (isZeroAmt) {
                            adapter.removeViewItem(pos);

                        } else {
                            adapter.updateViewItem(pos, item);
                        }

                        Log.e(TAG, "Data Updated....");
                    }
                }
            }
        }

    }

    private void scratchCardRedeem(ScratchItem scratchItem) {
        //showProgressBar();
        ScratchCardRedeemRequest request = new ScratchCardRedeemRequest(this);
        request.setScratchCard_id(scratchItem.getId());
        mAPIService = RetrofitClient.getAPIService(Constants.SCRATCH_CARD_BASE_URL);
        mCompositeDisposable.add(mAPIService.scratchCardRedeem(request).
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::scratchRedeemResponse, this::scratchRedeemError));

    }

    private void scratchRedeemError(Throwable throwable) {
        //closeProgressbar();
        Toast.makeText(this, "Error while uploading scratch data. ", Toast.LENGTH_SHORT)
                .show();
        Log.e(TAG, throwable.getLocalizedMessage());
    }

    private void scratchRedeemResponse(ScratchCardRedeemResponse scratchCardRedeemResponse) {
        //closeProgressbar();
        isFromRedeem = true;
        getScratchCardList();
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
