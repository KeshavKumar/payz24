package com.payz24.activities.recharge;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.payz24.R;
import com.payz24.activities.BaseActivity;

public class Electricity extends BaseActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electricity);
        toolbar = findViewById(R.id.action_toolbar);
        toolbar.setTitle(getString(R.string.electricity));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            if (true) {
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
            }
        }
    }
}
