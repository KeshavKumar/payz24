package com.payz24.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cooltechworks.views.ScratchTextView;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.payz24.R;
import com.payz24.fragment.ProfileFragment;
import com.payz24.responseModels.scratchCard.ScratchItem;
import com.payz24.utils.Constants;
import com.payz24.utils.PreferenceConnector;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ScratchCardFullActivity extends BaseActivity {

    final String TAG = "ScratchCardFullActivity";
    private ScratchItem mScratchItem;
    private ScratchTextView mUnScratchTextView;
    private ImageView mBackButton;
    private int mPosition;
    private TextView mTxtScratchView, mTxtCongratsMsg, mTxtPaidTo, mTxtPaidDate;
    private Button mBtnReferFriend;
    private LinearLayout mScratchLayout;
    private boolean isDataChanged = false;
    public static final  int REQUEST_INVITE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_scratch_card);

        getDataFromIntent();
        initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();

        setDataToView();
        setOnClickListeners();
    }

    void initUI() {
        mBackButton = findViewById(R.id.btnBackArrow);
        mUnScratchTextView = findViewById(R.id.txtUnScratchView);
        mTxtScratchView = findViewById(R.id.txtScratchView);
        mBtnReferFriend = findViewById(R.id.btnReferFriends);
        mScratchLayout = findViewById(R.id.scratchLayout);
        mTxtCongratsMsg = findViewById(R.id.txtCongratsMsg);
        mTxtPaidTo = findViewById(R.id.txtPaidTo);
        mTxtPaidDate = findViewById(R.id.txtPaidDate);
    }

    void setDataToView() {

        if (mScratchItem.isCardScratch()) {

            mUnScratchTextView.setVisibility(View.GONE);
            mScratchLayout.setVisibility(View.VISIBLE);
            mBtnReferFriend.setVisibility(View.VISIBLE);
            //mTxtScratchView.setText(getString(R.string.scratch_card_won) + "\n" + mScratchItem.getAmountEarn());
            if ((Integer.parseInt(mScratchItem.getAmountEarn()) > 0)) {
                mTxtScratchView.setText(getString(R.string.scratch_card_won) + "\n" + mScratchItem.getAmountEarn());
            } else {
                mTxtScratchView.setText(getString(R.string.scratch_card_empty));
            }
            mTxtCongratsMsg.setText(mScratchItem.getScratchMsg());
            mTxtPaidDate.setText(getString(R.string.paid_on) + " " + mScratchItem.getPaidDate());
            mTxtPaidTo.setText(getString(R.string.paying_msg) + " " + mScratchItem.getPaidTo());

        } else {

            mUnScratchTextView.setVisibility(View.VISIBLE);
            mScratchLayout.setVisibility(View.GONE);
            mBtnReferFriend.setVisibility(View.GONE);

            if ((Integer.parseInt(mScratchItem.getAmountEarn()) > 0)) {
                mUnScratchTextView.setText(getString(R.string.scratch_card_won) + "\n" + mScratchItem.getAmountEarn());
            } else {
                mUnScratchTextView.setText(getString(R.string.scratch_card_empty));
            }

            if (mUnScratchTextView != null) {
                mUnScratchTextView.setRevealListener(new ScratchTextView.IRevealListener() {
                    @Override
                    public void onRevealed(ScratchTextView tv) {
                        //Toast.makeText(ScratchCardFullActivity.this, "You got Rs. "
                        //      + mScratchItem.getAmountEarn(), Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "onRevealed  === ");
                    }

                    @Override
                    public void onRevealPercentChangedListener(ScratchTextView scratchTextView, float v) {
                        Log.e(TAG, "onRevealPercentChangedListener  === " + v);
                        if (v > 0.49) {
                            isDataChanged = true;
                            mScratchItem.setCardScratch(true);
                            mBtnReferFriend.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }

    }

    void setOnClickListeners() {

        mBackButton.setOnClickListener(v -> {
            sendDataToActivity();
        });

        mBtnReferFriend.setOnClickListener(v -> {
            //https://codesnipps.simolation.com/post/android/create-circular-reveal-animation-when-starting-activitys/
            //startActivity(new Intent(this, ScratchCardGeneratedActivity.class));
//            ScratchCardGenerationDialog dialog = new ScratchCardGenerationDialog();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            dialog.show(ft, ScratchCardGenerationDialog.TAG);

            referFriend();
        });
    }

    public static void sendGDMail(Context mContext, String mailId, String subject, String bodyContent) {

        final String appPackageName = "com.microsoft.office.outlook";

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);               // optional
        intent.putExtra(Intent.EXTRA_TEXT, bodyContent);                 // optional
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{mailId});      // optional
        intent.setType("text/plain");

        final PackageManager pm = mContext.getPackageManager();

        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);

        ResolveInfo best = null;

        //TODO Need to use activity name to call compose mail for outlook
        for (final ResolveInfo info : matches) {
            if (info.activityInfo.packageName.contains(appPackageName)) {
                best = info;
            }
        }

        if (best != null) {
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
            mContext.startActivity(intent);

        } else {
            //if outlook is not available redirecting to playstore link for outlook
            try {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            Log.e("sendGDMail", "Email client not found");
        }
    }

    void referFriend() {

        try {
            /*Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.trophy);
            Uri bmpUri = getLocalBitmapUri(bitmap);
            String refer = PreferenceConnector.readString(this, getString(R.string.promo), "");
            String sAux = "";
            int amt = Integer.parseInt(mScratchItem.getAmountEarn());
            if (amt != 0) {
                sAux = String.format(getResources().getString(R.string.referral_msg),
                        mScratchItem.getAmountEarn(), refer);
            } else {
                sAux = String.format(getResources().getString(R.string.referral_msg_empty),
                        refer);
            }
            sAux = sAux + " " + Constants.GOOGLE_PLAYSTORE_URL;

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);

            sendIntent.putExtra(Intent.EXTRA_TEXT, sAux);
            sendIntent.setType("text/plain");

            sendIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            sendIntent.setType("image/png");

            sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Intent.createChooser(sendIntent, "Choose one");
            startActivity(sendIntent);*/

            Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                    .setMessage(getString(R.string.invitation_message))
                    .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))

                    .build();
            startActivityForResult(intent, REQUEST_INVITE);

        } catch (Exception e) {
            Toast.makeText(this, R.string.status_error, Toast.LENGTH_SHORT).show();
        }

    }


    private Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bmpUri = Uri.fromFile(file);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return bmpUri;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            mScratchItem = (ScratchItem) intent.getSerializableExtra(ScratchCardActivity.INTENT_SCRATCH_CARD);
            mPosition = intent.getIntExtra(ScratchCardActivity.INTENT_SCRATCH_CARD_POISTION, 0);
        }
    }

    @Override
    public void onBackPressed() {
        sendDataToActivity();
    }

    void sendDataToActivity() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(ScratchCardActivity.INTENT_SCRATCH_CARD, mScratchItem);
        returnIntent.putExtra(ScratchCardActivity.INTENT_SCRATCH_CARD_POISTION, mPosition);
        int earnAmt = Integer.parseInt(mScratchItem.getAmountEarn().trim());
        returnIntent.putExtra(ScratchCardActivity.INTENT_EMPTY_SCRATCH_CARD, (earnAmt > 0 ? false : true));
        returnIntent.putExtra(ScratchCardActivity.INTENT_SCRATCH_CARD_CHANGED, isDataChanged);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                Log.e(TAG, "onActivityResult: sent invitation success ");
            } else {
                Log.e(TAG, "onActivityResult: sent invitation failed ");
            }
        }
    }

}
