package com.payz24.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.payz24.R;
import com.payz24.responseModels.scratchCard.ScratchItem;
import com.payz24.utils.Constants;

import java.util.List;

/**
 * Created by someo on 16-05-2018.
 */

public class ScratchAdapter extends RecyclerView.Adapter<ScratchAdapter.MyViewHolder> {

    private Context mContext;
    private List<ScratchItem> mScratchItemList;
    private OnCardViewClickListener mListener;

    public ScratchAdapter(Context mContext, List<ScratchItem> scratchItemList, OnCardViewClickListener listener) {
        this.mContext = mContext;
        this.mScratchItemList = scratchItemList;
        mListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_setting, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ScratchItem scratchItem = mScratchItemList.get(position);
        holder.scratchTextView.setText(scratchItem.getAmountEarn());

        //adding different images
        if (scratchItem.getPaidType().equalsIgnoreCase(Constants.SCRATCH_CARD_RECHARGE)) {
            holder.thumbnail.setImageResource(R.drawable.ic_recharge);
        } else if (scratchItem.getPaidType().equalsIgnoreCase(Constants.SCRATCH_CARD_FLIGHT)) {
            holder.thumbnail.setImageResource(R.drawable.ic_flights);
        } else if (scratchItem.getPaidType().equalsIgnoreCase(Constants.SCRATCH_CARD_BUS)) {
            holder.thumbnail.setImageResource(R.drawable.ic_bus);
        }

        if (scratchItem.isCardScratch()) {
            holder.imgUnScratch.setVisibility(View.GONE);
            holder.layScratchCard.setVisibility(View.VISIBLE);
        } else {
            holder.imgUnScratch.setVisibility(View.VISIBLE);
            holder.layScratchCard.setVisibility(View.GONE);
        }

        holder.cardView.setOnClickListener((v) -> {
            mListener.onCardViewClick(scratchItem, position);
        });


    }

    @Override
    public int getItemCount() {
        return mScratchItemList.size();
    }

    public void removeViewItem(int position) {
        mScratchItemList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mScratchItemList.size());
    }

    public void updateViewItem(int position, ScratchItem scratchItem) {
        mScratchItemList.set(position, scratchItem);
        notifyItemChanged(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView thumbnail;
        private View imgUnScratch;
        private TextView scratchTextView;
        private CardView cardView;
        private LinearLayout layScratchCard;

        public MyViewHolder(View view) {
            super(view);
            scratchTextView = view.findViewById(R.id.txtAmount);
            thumbnail = view.findViewById(R.id.thumbnail);
            cardView = view.findViewById(R.id.card_view);
            imgUnScratch = view.findViewById(R.id.imgUnScratch);
            layScratchCard = view.findViewById(R.id.layScratchCard);

        }

    }

    public interface OnCardViewClickListener {
        void onCardViewClick(ScratchItem scratchItem, int position);

    }
}